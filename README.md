# Webarchitects Microsoft Edge Ansible role

[![pipeline status](https://git.coop/webarch/edge/badges/master/pipeline.svg)](https://git.coop/webarch/edge/-/commits/master)

An Ansible role to install the Microsoft Edge browser on Debian and Ubuntu.

## Role variables

See the [defaults/main.yml](defaults/main.yml) file for the default variables and [meta/argument_spacs.yml](meta/argument_specs.yml) for the variable specification.

### edge

A boolean, by default the `edge` variable is set to `false` to prevent any tasks in this role being run.

### edge_firejail

A boolean, running Microsoft Edge in [firejail](https://github.com/netblue30/firejail) doesn't currently work so this defaults to `false`.

### edge_versions

A list of one of more versions of Microsoft Edge to install, the package names are:

- `microsoft-edge-beta`
- `microsoft-edge-dev`
- `microsoft-edge-stable`

## Repository

The primary URL of this repo is [`https://git.coop/webarch/edge`](https://git.coop/webarch/edge) however it is also [mirrored to GitHub](https://github.com/webarch-coop/ansible-role-edge) and [available via Ansible Galaxy](https://galaxy.ansible.com/chriscroome/edge).

If you use this role please use a tagged release, see [the release notes](https://git.coop/webarch/edge/-/releases). 

## Copyright

Copyright 2019-2023 Chris Croome, &lt;[chris@webarchitects.co.uk](mailto:chris@webarchitects.co.uk)&gt;.

This role is released under [the same terms as Ansible itself](https://github.com/ansible/ansible/blob/devel/COPYING), the [GNU GPLv3](LICENSE).
